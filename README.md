use nix
-------

The `shellHook` *does not* execute when using the cached derivation.

```sh
(nix-direnv-shellhook-demo) nix-direnv-shellhook-demo master ✔ $ direnv deny
direnv: error /home/user/nix-direnv-shellhook-demo/.envrc is blocked. Run `direnv allow` to approve its content
nix-direnv-shellhook-demo master ✔ $ rm -rf .direnv
nix-direnv-shellhook-demo master ✔ $ echo "use nix" > .envrc
direnv: error /home/user/nix-direnv-shellhook-demo/.envrc is blocked. Run `direnv allow` to approve its content
nix-direnv-shellhook-demo master ✚1 $ direnv allow
direnv: loading ~/projects/nix-direnv-shellhook-demo/.envrc
direnv: using nix
SHELLHOOK
direnv: eval /home/user/nix-direnv-shellhook-demo/.direnv/cache-.4244.47e580e291f
direnv: renewed cache and derivation link
direnv: export +AR +AS +CC +CONFIG_SHELL +CXX +HOST_PATH +IN_NIX_SHELL +LD +NIX_BINTOOLS +NIX_BINTOOLS_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_BUILD_CORES +NIX_BUILD_TOP +NIX_CC +NIX_CC_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_ENFORCE_NO_NATIVE +NIX_HARDENING_ENABLE +NIX_INDENT_MAKE +NIX_LDFLAGS +NIX_STORE +NM +OBJCOPY +OBJDUMP +RANLIB +READELF +SIZE +SOURCE_DATE_EPOCH +STRINGS +STRIP +TEMP +TMP +TMPDIR +__ETC_PROFILE_SOURCED +buildInputs +builder +configureFlags +depsBuildBuild +depsBuildBuildPropagated +depsBuildTarget +depsBuildTargetPropagated +depsHostHost +depsHostHostPropagated +depsTargetTarget +depsTargetTargetPropagated +doCheck +doInstallCheck +name +nativeBuildInputs +nobuildPhase +out +outputs +patches +phases +propagatedBuildInputs +propagatedNativeBuildInputs +shell +shellHook +stdenv +strictDeps +system ~PATH ~XDG_DATA_DIRS
(nix-direnv-shellhook-demo) nix-direnv-shellhook-demo master ✚1 $ cd ..
direnv: unloading
projects  $ cd nix-direnv-shellhook-demo
direnv: loading ~/projects/nix-direnv-shellhook-demo/.envrc
direnv: using nix
direnv: using cached derivation
direnv: eval /home/user/nix-direnv-shellhook-demo/.direnv/cache-.4244.47e580e291f
direnv: export +AR +AS +CC +CONFIG_SHELL +CXX +HOST_PATH +IN_NIX_SHELL +LD +NIX_BINTOOLS +NIX_BINTOOLS_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_BUILD_CORES +NIX_BUILD_TOP +NIX_CC +NIX_CC_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_ENFORCE_NO_NATIVE +NIX_HARDENING_ENABLE +NIX_INDENT_MAKE +NIX_LDFLAGS +NIX_STORE +NM +OBJCOPY +OBJDUMP +RANLIB +READELF +SIZE +SOURCE_DATE_EPOCH +STRINGS +STRIP +TEMP +TMP +TMPDIR +__ETC_PROFILE_SOURCED +buildInputs +builder +configureFlags +depsBuildBuild +depsBuildBuildPropagated +depsBuildTarget +depsBuildTargetPropagated +depsHostHost +depsHostHostPropagated +depsTargetTarget +depsTargetTargetPropagated +doCheck +doInstallCheck +name +nativeBuildInputs +nobuildPhase +out +outputs +patches +phases +propagatedBuildInputs +propagatedNativeBuildInputs +shell +shellHook +stdenv +strictDeps +system ~PATH ~XDG_DATA_DIRS
```

use flake
---------

The `shellHook` *does* execute when using cached dev shell.

```sh
(nix-direnv-shellhook-demo) nix-direnv-shellhook-demo master ✚1 $ direnv deny
direnv: error /home/user/nix-direnv-shellhook-demo/.envrc is blocked. Run `direnv allow` to approve its content
nix-direnv-shellhook-demo master ✚1 $ rm -rf .direnv
nix-direnv-shellhook-demo master ✚1 $ echo "use flake" > .envrc
direnv: error /home/user/nix-direnv-shellhook-demo/.envrc is blocked. Run `direnv allow` to approve its content
nix-direnv-shellhook-demo master ✚1 $ direnv allow
direnv: loading ~/projects/nix-direnv-shellhook-demo/.envrc
direnv: using flake
warning: Git tree '/home/user/nix-direnv-shellhook-demo' is dirty
[warning](warning): Git tree '/home/user/nix-direnv-shellhook-demo' is dirty
direnv: renewed cache
SHELLHOOK
direnv: export +AR +AS +CC +CONFIG_SHELL +CXX +HOST_PATH +IN_NIX_SHELL +LD +NIX_BINTOOLS +NIX_BINTOOLS_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_BUILD_CORES +NIX_CC +NIX_CC_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_CFLAGS_COMPILE +NIX_ENFORCE_NO_NATIVE +NIX_HARDENING_ENABLE +NIX_INDENT_MAKE +NIX_LDFLAGS +NIX_STORE +NM +OBJCOPY +OBJDUMP +RANLIB +READELF +SIZE +SOURCE_DATE_EPOCH +STRINGS +STRIP +buildInputs +builder +configureFlags +depsBuildBuild +depsBuildBuildPropagated +depsBuildTarget +depsBuildTargetPropagated +depsHostHost +depsHostHostPropagated +depsTargetTarget +depsTargetTargetPropagated +doCheck +doInstallCheck +dontAddDisableDepTrack +name +nativeBuildInputs +nobuildPhase +out +outputs +patches +phases +propagatedBuildInputs +propagatedNativeBuildInputs +shell +shellHook +stdenv +strictDeps +system ~PATH ~XDG_DATA_DIRS
(nix-direnv-shellhook-demo) nix-direnv-shellhook-demo master ✚1 $ cd ..
direnv: unloading
projects  $ cd nix-direnv-shellhook-demo
direnv: loading ~/projects/nix-direnv-shellhook-demo/.envrc
direnv: using flake
direnv: using cached dev shell
SHELLHOOK
direnv: export +AR +AS +CC +CONFIG_SHELL +CXX +HOST_PATH +IN_NIX_SHELL +LD +NIX_BINTOOLS +NIX_BINTOOLS_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_BUILD_CORES +NIX_CC +NIX_CC_WRAPPER_TARGET_HOST_x86_64_unknown_linux_gnu +NIX_CFLAGS_COMPILE +NIX_ENFORCE_NO_NATIVE +NIX_HARDENING_ENABLE +NIX_INDENT_MAKE +NIX_LDFLAGS +NIX_STORE +NM +OBJCOPY +OBJDUMP +RANLIB +READELF +SIZE +SOURCE_DATE_EPOCH +STRINGS +STRIP +buildInputs +builder +configureFlags +depsBuildBuild +depsBuildBuildPropagated +depsBuildTarget +depsBuildTargetPropagated +depsHostHost +depsHostHostPropagated +depsTargetTarget +depsTargetTargetPropagated +doCheck +doInstallCheck +dontAddDisableDepTrack +name +nativeBuildInputs +nobuildPhase +out +outputs +patches +phases +propagatedBuildInputs +propagatedNativeBuildInputs +shell +shellHook +stdenv +strictDeps +system ~PATH ~XDG_DATA_DIRS
```

nix develop
-----------

The `nix develop` command does cache yet runs the shellHook anyway.

```sh
(nix-direnv-shellhook-demo) nix-direnv-shellhook-demo master ✔ $ direnv deny
direnv: error /home/user/nix-direnv-shellhook-demo/.envrc is blocked. Run `direnv allow` to approve its content
nix-direnv-shellhook-demo master ✔ $ nix develop
warning: Git tree '/home/user/nix-direnv-shellhook-demo' is dirty
SHELLHOOK
user@machine nix-direnv-shellhook-demo [] $ exit
exit
nix-direnv-shellhook-demo master ✔ $ nix develop
warning: Git tree '/home/user/nix-direnv-shellhook-demo' is dirty
SHELLHOOK
```
